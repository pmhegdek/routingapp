import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProjectListComponent } from './projects/project-list.component';
import { ProjectDetailsComponent } from './projects/project-details.component'
import { BeaconDetailsComponent } from './beacons/beacon-details.component'
import { HomeComponent } from './home/home.component'
import { NewProjectComponent } from './new-project/new-project.component'
import { NewBeaconComponent } from './new-beacon/new-beacon.component'


//Route configuration
export const routes: Routes = [
    { path: 'projects', component: ProjectListComponent },
    { path: 'projects/:project_id', component: ProjectDetailsComponent },
    { path: 'projects/edit/new', component: NewProjectComponent },
    { path: 'projects/:project_id/beacons/:beacon_id', component: BeaconDetailsComponent },
    { path: 'projects/:project_id/beacons/edit/new', component: NewBeaconComponent },
    { path: 'home', component: HomeComponent },
    { path: '**', component: HomeComponent }
];

export const routing: ModuleWithProviders = RouterModule.forRoot(routes);