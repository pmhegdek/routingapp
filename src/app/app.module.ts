import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule, JsonpModule } from '@angular/http';


//declarations
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component'
import { ProjectListComponent } from './projects/project-list.component';
import { ProjectDetailsComponent } from './projects/project-details.component';
import { BeaconDetailsComponent } from './beacons/beacon-details.component';
import { NewProjectComponent } from './new-project/new-project.component'
import { NewBeaconComponent } from './new-beacon/new-beacon.component'

import { routing } from './app.routes';

@NgModule({
  declarations: [
    AppComponent,
    ProjectListComponent,
    ProjectDetailsComponent,
    BeaconDetailsComponent,
    NewProjectComponent,
    NewBeaconComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    JsonpModule,
    routing
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
