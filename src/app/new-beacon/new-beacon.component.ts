import { Component } from '@angular/core'
import { Router, ActivatedRoute } from '@angular/router'

@Component({
    templateUrl: './new-beacon.component.html',
    styleUrls: ['./new-beacon.component.css']
})

export class NewBeaconComponent {

    projectId: string;
    beacon:Object;
    constructor(private router: Router, private activatedRoute:ActivatedRoute) {
        this.beacon={
            name:null,
            details:null
        }
    }
    ngOnInit() {
        this.activatedRoute.params.subscribe(params => {
            this.projectId = params['project_id'];
            //this.beacons=
            console.log("project id:" + this.projectId);
        });
    }

    onCancelButtonClick() {
        this.router.navigate(["projects/"+this.projectId]);
    }

}