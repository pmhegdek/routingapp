import { Component } from '@angular/core';
import { Http, RequestOptions, Headers } from '@angular/http'
import { ActivatedRoute, Router } from '@angular/router';

@Component({
    templateUrl: './beacon-details.component.html',
    styleUrls: ['./beacon-details.component.css']
})

export class BeaconDetailsComponent {

    projectId: string;
    beaconId: string;
    beaconDetails: Object;
    constructor(public http: Http, private activatedRoute: ActivatedRoute, private router: Router) {

        this.beaconDetails = {
            name: null,
            details: null,
            URLs: [{ name: null, url: null }]
        };
    }
    ngOnInit() {
        this.activatedRoute.params.subscribe(params => {
            this.projectId = params['project_id'];
            this.beaconDetails = params['beacon_id'];
            this.beaconDetails = {
                name: "beacon 1 dummy data",
                id: this.beaconId,
                details: "dummy details dummy details dummy details dummy details",
                URLs: [
                    { name: "URL 1", url: "www.google.com" },
                    { name: "URL 2", url: "www.facebook.com" },
                    { name: "URL 3", url: "www.linkedin.com" }]
            };
            //this.beacons=
            console.log("project id:" + this.projectId);
            this.getThisBeaconDetails();
        });
    }

    getThisBeaconDetails() {
        // console.log("download beacon details");
        // this.http.get("your url goes here")
        //     .map(response => response.json()).subscribe(
        //         function(response){
        //             //success
        //         },function(err){
        //             //err
        //         },function(){
        //             //complete
        //         }
        //     );
    }
    onClickURL(event,url){
        
    }
}