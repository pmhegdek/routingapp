import { Component } from '@angular/core'
import { Router } from '@angular/router';


@Component({
    templateUrl: './new-project.component.html',
    styleUrls: ['./new-project.component.css']
})

export class NewProjectComponent {
    constructor(private router: Router) { }

    project: Object;

    ngOnInit() { 
        //this.project={name:"beacon name"};
    }
    onSubmit() { }
    onCancelButtonClick() {
        this.router.navigate(['projects/']);
    }
}