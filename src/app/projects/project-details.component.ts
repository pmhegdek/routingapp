import { Component } from '@angular/core';
import { Http, RequestOptions, Headers } from '@angular/http'
import { ActivatedRoute, Router } from '@angular/router';


@Component({
    templateUrl: './project-details.component.html',
    styleUrls: ['./project-details.component.css']
})

//component class
export class ProjectDetailsComponent {

    projectId: string;
    projectDetails: Object;
    //beacons: Array<Object>;


    constructor(public http: Http, private activatedRoute: ActivatedRoute, private router: Router) {
    }

    ngOnInit() {
        console.log("init download project details from the server");
        this.activatedRoute.params.subscribe(params => {
            this.projectId = params['project_id'];
            this.projectDetails = {
                name: "dummy data",
                id: this.projectId,
                details: "dummy details dummy details dummy details dummy details",
                beacons: [
                    { name: "Beacon 1", id: "34" },
                    { name: "Beacon 2", id: "31" },
                    { name: "Beacon 3", id: "35" }]
            };
            //this.beacons=
            console.log("project id:" + this.projectId);
            this.getThisProjectDetails();
        });
    }

    getThisProjectDetails() {
        console.log("get this project details");

        const theaders = new Headers();
        theaders.append('Access-Control-Allow-Headers', 'Content-Type');
        theaders.append('Access-Control-Allow-Methods', 'GET');
        theaders.append('Access-Control-Allow-Origin', '127.0.0.1:4200');

        let options = new RequestOptions({ headers: theaders });

        this.http.get("http://dev.gurusangeet.com/users/usha")
            .map(response => response.json()).subscribe(
            function (response) {
                console.log("Success");
                if (response.details != null) {
                    this.projectDetails = response.details;
                } else {
                    this.projectDetails = {
                        name: "dummy data",
                        id: this.projectId,
                        details: "dummy details dummy details dummy details dummy details",
                        beacons: [
                            { name: "Beacon 1", id: "34" },
                            { name: "Beacon 2", id: "31" },
                            { name: "Beacon 3", id: "35" }]
                    };
                }
            },
            function (err) {
                err => console.log("error: " + err);
            },
            function () {
                console.log("Subscription completed");
            });
    }
    onClickBeacon(event, beacon_id) {
        console.log("Beacon on click: " + beacon_id);
        this.router.navigate(["projects/" + this.projectId + "/beacons/" + beacon_id]);
    }
    onAddNewBeaconClicked(){
        this.router.navigate(["projects/"+this.projectId+"/beacons/edit/new"]);
    }
}