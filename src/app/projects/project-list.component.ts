import { Component } from '@angular/core';
import { Http, RequestOptions, Headers } from '@angular/http';
import { Router } from '@angular/router';


import 'rxjs/add/operator/map'

@Component({
    templateUrl: './project-list.component.html',
    styleUrls: ['./project-list.component.css']
})

//component class
export class ProjectListComponent {

    proj1 = {
        name: "project1",
        id: "1234",
        details: "project1 project1 project1 project1 project1 project1project1 project1 project1project1 project1 project1"
    };

    proj2 = {
        name: "project2",
        id: "3456",
        details: "project2 project2 project2 project2 project2 project2 project2 project2 project2"
    }
    myProjects: Array<Object> = [this.proj1, this.proj2];

    constructor(public http: Http, private router: Router) {
    }

    ngOnInit() {
        console.log("download project list ");
        this.getMyProjects();
    }

    getMyProjects() {
        const theaders = new Headers();
        theaders.append('Access-Control-Allow-Headers', 'Content-Type');
        theaders.append('Access-Control-Allow-Methods', 'GET');
        theaders.append('Access-Control-Allow-Origin', '127.0.0.1:4200');

        let options = new RequestOptions({ headers: theaders });

        this.http.get("http://dev.gurusangeet.com/users/usha", options)
            .map(response => response.json())
            .subscribe(
            function (response) {
                if (response.projects != null) {
                    console.log("projects not null");
                    this.myProjects = response.projects;
                } else {
                    console.log("response data null");
                }
            },
            function (error) {
                error => console.log("Error" + error);
            },
            function () {
                console.log("completed");
            });
        console.log("Projects: " + this.myProjects)
    }

    onClickProject(event, project_id) {
        console.log("project on click" + project_id);
        this.router.navigate(["projects/" + project_id]);
    }

    onClickNewProject() {
        this.router.navigate(["projects/edit/new"]);
    }
}